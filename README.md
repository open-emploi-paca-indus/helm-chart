# Démo de charts Helm pour la stack dev-datapoc2

Compilation de charts minimalistes et transposables pour les différents 
composants de la stack Synaptix.

Les fichiers de values de chaque chart contiennent des valeurs minimales
recommandées pour les ressources à allouer à chaque composant.

Il est nécessaire d'adapter ses ressources à l'empleur des jeux de données
traités.

## Pré-requis

- helm : [https://helm.sh](https://helm.sh)
- helmfile : [https://github.com/helmfile/helmfile](https://github.com/helmfile/helmfile)
- le plugin helm-diff : [https://github.com/databus23/helm-diff](https://github.com/databus23/helm-diff)

Pour la partie gestions des secrets se référer au plugin [helm-secrets](https://github.com/jkroepke/helm-secrets).

## Lancement

```bash
helmfile -i apply
```

## Tout supprimer

```bash
helmfile -i delete
```

## Mot de passe de la clé GPG

`demo`

## Variables d'environnement

Les variables d'environnement sont à spécifier dans le fichier secret et à mapper dans les fichiers .yaml